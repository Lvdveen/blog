---
title: Week 2
subtitle: Maandag 11-09-2017 t/m Vrijdag 15-09-2017
date: 2017-09-15
tags: ["week2", "blog"]
---
Maandag *11-09-2017*
---
In de ochtend hebben we de teamafspraken en doelen van elkaar nogmaals besproken en doorgenomen, daarna hebben een korte brainstormsessie gehouden over hoe we het paperprototype kunnen maken voor ons project.
We hebben verschillende ideeen besproken en kwamen tot de conclusie dat een kleine kaart met gebouwen een goede oplossing was voor ons spel. We hebben kleine gebouwen op een kaart geplakt en dat stelt Rotterdam voor.
![map](../img/map.png)

Dinsdag *12-09-2017*
---
Op deze ochtend heb ik aan het hoorcollege van Verbeelden deelgenomen. Hier heb ik veel kennis op gedaan wat ik later nog nodig zal hebben voor het eerste tentamen.

Woensdag *13-09-2017*
---
De dag was het de bedoeling dat iedereen met zijn groepje een korte presentatie ging geven over hun idee. Wij hebben onze kaart van Rotterdam neergelegd en een aantal schetsen van hoe het spel erop de telefoon uit zal zien.
Hier bij heb ik ook een aantal keer korte presentaties gegeven aan klasgenoten en docenten om ons idee over te brengen.
Ons idee was al best ver uitgewerkt dus er was weinig goede feedback. Ook heb ik een kijkje genomen bij de andere groepen om inspiratie op te doen.

Donderdag *14-09-2017*
---
Deze dag had ik de eerste les van het keuzevak Photoshop. Ik heb al kennis van photoshop dus deze les was vrij standaard.
In de middag hadden wij een werkcollege over Verbeelden, hier hebben wij verschillende vormen van de gestallt-wetten uit zelfmeegenomen foto's besproken.
Na het werkcollege hebben we alle opdrachten afgerond en in orde gemaakt om het vervolgens te kunnen inleveren voor de deadline van vrijdag.

Vrijdag *15-09-2017*
---
