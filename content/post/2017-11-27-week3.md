---
title: Week 3
subtitle: Maandag 27-11-2017 t/m Vrijdag 01-12-2017
date: 2017-11-27
tags: ["week3", "blog"]
---
Maandag *27-11-2017*
---
Ik ben verder gegaan met de user journey omdat ik deze graag wilden valideren voor in mijn leerdossier. Ik heb veel dingen aan mijn team gevraagt voordat ik echt helemaal klaar was.
Dit kost meer tijd dan ik in eerste instantie in gedachten had.

Dinsdag *28-11-2017*
---


Woensdag *29-11-2017*
---
Ik heb me niet bezig gehouden met de pitch die volgende week gepland staat. Ik heb wel geholpen met enkele visual die we konden gebruiken voor de pitch.

Donderdag *30-11-2017*
---


Vrijdag *01-12-2017*
---