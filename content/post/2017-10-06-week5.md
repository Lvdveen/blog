---
title: Week 5
subtitle: Maandag 02-10-2017 t/m Vrijdag 06-10-2017
date: 2017-10-09
tags: ["week5", "blog"]
---
Maandag *02-10-2017*
---
Deze dag heb ik de spelanalyses van mijn spellen (en deels van Indy) gemaakt. Ik heb voor mijn eigen spelanalyses heb ik de volgende spellen gebruikt: TicTacToe en Scribbl.
Ik heb deze in photoshop gerealiseerd en heb vervolgens Indy geholpen met zijn spelanalyses. Deze waren voor Monopoly en Mens-Erger-Je-Niet en die heb ik ook visueel gemaakt samen met Indy zodat we met z'n alle efficient door konden werken.
Daarna hebben we de visuals gemaakt voor de presentatie van de app. Deze hebben Indy, Rick en Ik samen gemaakt.
Ook hebben we een peerfeedback op de samenwerking's formulier ingevuld voor elkaar. Ik heb de feedback in me opgenomen en zal eraan gaan werken.

Dinsdag *03-10-2017*
---
Beroepsprofiel gemaakt doormiddel van informatie verzamelen over CMD. De laatste visuals gemaakt voor de presentatie.

Woensdag *04-10-2017*
---
In de ochtend hadden wij de presentatie gehouden voor de docenten en de alumni, wij hadden ons idee op een zo goed mogelijke manier overgebracht aan de luisteraar.
De presentatie heb ik gehouden met Indy en Kim en kregen daar wisselende feedback op. 2 mensen gaven erg tegenstrijdige feedback en hebben dit op een formulier tot ons genomen.
Na de presentatie kwamen we tot de conclusie dat we een spelobservatie misten, dit kwam omdat we niet echt een speelbare versie van het spel hadden. We kwamen op het idee om de marvelapp te gebruiken, om de app speelbaar te maken.
Als alle bestanden verzamelt zijn, was het klaar om in te leveren.

Donderdag *05-10-2017*
---


Vrijdag *06-10-2017*
---